if ('WebSocket' in window) {
    const urlParameter = new URLSearchParams(window.location.search);

    const ws_stomp = new SockJS('http://152.118.148.95:15674/stomp');
    const client = Stomp.over(ws_stomp);
    client.debug = () => null;
    const mq_queue = `/exchange/1706067626/${urlParameter.get('key')}`;
    let fileCounter = 0;

    const onMessageCallback = (message) => {
        const messages = message.body.split('-');
        if (messages[0] === 'compressing') {
            const sizeProgress = document.getElementById('size-progress');
            sizeProgress.style.width = `${messages[1]}%`;
            sizeProgress.innerText = `${messages[1]}%`;
            const countProgress = document.getElementById('count-progress');
            if (messages.length === 5) {
                fileCounter = messages[4]
            }
            countProgress.style.width = `${(fileCounter)}%`;
            countProgress.innerText = `${(fileCounter)}%`;
        } else {
            const button = document.getElementById('download-button');
            button.setAttribute('href', `http://infralabs.cs.ui.ac.id:20063/media/download?id=${messages[1]}`);
            button.removeAttribute('style')
        }
    };
    const connectedCallback = () => {
        client.subscribe(mq_queue, onMessageCallback);
    };
    const errorCallback = () => {
    };
    client.connect('0806444524', '0806444524', connectedCallback, errorCallback, '/0806444524');
} else {
    console.log('websocket does not exist');
}
