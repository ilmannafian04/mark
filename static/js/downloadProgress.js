if ('WebSocket' in window) {
    const urlParameter = new URLSearchParams(window.location.search);

    const ws_stomp = new SockJS('http://152.118.148.95:15674/stomp');
    const client = Stomp.over(ws_stomp);
    client.debug = () => null;
    const mq_queue = `/exchange/1706067626/${urlParameter.get('key')}`;

    const onMessageCallback = (message) => {
        const messages = message.body.split('-')
        if (messages[0] === 'downloading') {
            const sizeProgress = document.getElementById('size-progress');
            const percentage = Number(messages[1]);
            sizeProgress.style.width = `${percentage.toFixed(1)}%`;
            sizeProgress.innerText = `${percentage.toFixed(1)}%`;
        } else if (messages[0] === 'done') {
            const downloadButton = document.getElementById('download-button');
            downloadButton.setAttribute(
                'href',
                `/downloader/download?id=${messages[1]}`
            );
            downloadButton.classList.remove('disabled')
        }
    };
    const connectedCallback = () => {
        client.subscribe(mq_queue, onMessageCallback);
    };
    const errorCallback = () => {
    };
    client.connect('0806444524', '0806444524', connectedCallback, errorCallback, '/0806444524');
} else {
    console.log('websocket does not exist');
}
