let fileCounter = 1;

function changeName(input) {
    const path = input.value.split('\\');
    const id = input.id.split('-');
    document.getElementById(`file-input-text${id[id.length - 1]}`).innerText = path[path.length - 1];
}

function addField() {
    const container = document.getElementById('file-fields');
    const fieldContainer = document.createElement('div');
    fieldContainer.classList.add('form-group');
    const customFileContainer = document.createElement('div');
    customFileContainer.id = `file-container${++fileCounter}`;
    customFileContainer.classList.add('custom-file');
    const fileInput = document.createElement('input');
    fileInput.type = 'file';
    fileInput.classList.add('custom-file-input');
    fileInput.id = `customFile-${fileCounter}`;
    fileInput.name = `file${fileCounter}`;
    fileInput.onchange = function(){changeName(fileInput)};
    const fileInputText = document.createElement('label');
    fileInputText.id = `file-input-text${fileCounter}`;
    fileInputText.classList.add('custom-file-label');
    fileInputText.for = `customFile-${fileCounter}`;
    fileInputText.innerText = 'Choose file';
    customFileContainer.appendChild(fileInput);
    customFileContainer.appendChild(fileInputText);
    fieldContainer.appendChild(customFileContainer);
    container.appendChild(fieldContainer);
}
