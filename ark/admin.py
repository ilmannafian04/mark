from django.contrib import admin

from ark.models import DownloadStorage


class DownloadStorageAdmmin(admin.ModelAdmin):
    pass


admin.site.register(DownloadStorage, DownloadStorageAdmmin)
