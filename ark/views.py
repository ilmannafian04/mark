import json
import logging
import mimetypes
import time
import uuid
from datetime import datetime

import after_response
import pika
import requests
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.http import HttpResponseNotFound, HttpResponseBadRequest, FileResponse, HttpResponseForbidden, \
    HttpResponse, HttpResponseServerError
from django.shortcuts import render, redirect

from ark.forms import TimeCalculatorForm, OAuthForm, FileUploadForm
from ark.models import FileStorage, DownloadStorage

logger = logging.getLogger('logstash')
fib_cache = {0: 0, 1: 1}


def downloader(request):
    if request.method == 'GET':
        return render(request, 'Downloader.html')
    elif request.method == 'POST':
        if 'download_url' in request.POST and 'filename' in request.POST:
            key = str(uuid.uuid4())
            downloader_after_response.after_response(request.POST['download_url'], request.POST['filename'], key)
            return redirect(f'/downloader/progress?key={key}')
        else:
            return redirect('downloader')
    else:
        return HttpResponseNotFound()


@after_response.enable
def downloader_after_response(url, filename, key):
    time.sleep(1)
    credentials = pika.PlainCredentials(settings.RABBITMQ_USER, settings.RABBITMQ_PASSWORD)
    parameters = pika.ConnectionParameters(
        settings.RABBITMQ_HOST,
        settings.RABBITMQ_PORT,
        settings.RABBITMQ_VHOST,
        credentials
    )
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.exchange_declare(exchange=settings.NPM, exchange_type='direct')
    queue = channel.queue_declare(queue=key)
    channel.queue_bind(exchange=settings.NPM, queue=queue.method.queue)

    response = requests.get(url, stream=True)
    response_length = response.headers.get('content-length')
    if response_length:
        downloaded = 0
        response_length = int(response_length)
        content = None
        for data in response.iter_content(chunk_size=response_length//1000):
            if downloaded == 0:
                content = data
            else:
                content += data
            downloaded += len(data)
            channel.basic_publish(
                exchange=settings.NPM,
                routing_key=key,
                body=f'downloading-{downloaded * 100 / response_length}-{response_length}'
            )
        if len(filename.split('.')) == 1 and 'content-type' in response.headers:
            filename += mimetypes.guess_extension(response.headers["content-type"].split(';')[0])
        result = SimpleUploadedFile(filename, content)
        downloaded_file = DownloadStorage(file=result)
    else:
        downloaded_file = DownloadStorage(file=response.content)
    downloaded_file.save()
    channel.basic_publish(
        exchange=settings.NPM,
        routing_key=key,
        body=f'done-{downloaded_file.id}'
    )
    connection.close()


def downloader_progress(request):
    if request.method == 'GET':
        return render(request, 'DownloaderProgress.html')
    else:
        return HttpResponseNotFound()


def downloader_download(request):
    if request.method == 'GET':
        if 'id' in request.GET:
            query_id = request.GET['id']
            try:
                query = DownloadStorage.objects.get(id=query_id)
            except DownloadStorage.DoesNotExist:
                return HttpResponseNotFound()
            return FileResponse(query.file, as_attachment=True)
        else:
            return HttpResponseBadRequest()
    else:
        return HttpResponseNotFound()


def fibonacci(request):
    logger.debug('views handler called')
    logger.debug(f'request method is {request.method}')
    if request.method == 'GET':
        if 'n' in request.GET:
            n = int(request.GET['n'])
            logger.debug(f'request with parameter n={n}')
            if n >= 0:
                result = fibonacci_rec(n)
                logger.debug(f'{n}-(st/nd/th) value of fibonacci number is {result}')
                context = {'result': result, 'input': n}
                logger.debug('rendering result page')
                return render(request, 'fibonacci.html', context)
            else:
                logger.debug('no negative fibonacci number')
                return HttpResponseBadRequest()
        else:
            logger.debug('rendering form page')
            return render(request, 'fibonacci.html')
    else:
        logger.debug(f'request with method {request.method} does not have a handler, throwing 404')
        return HttpResponseNotFound()


def fibonacci_rec(n):
    if n in fib_cache:
        return fib_cache[n]
    else:
        if n - 1 in fib_cache:
            a = fib_cache[n - 1]
        else:
            a = fibonacci_rec(n - 1)
            fib_cache[n - 1] = a
        if n - 2 in fib_cache:
            b = fib_cache[n - 2]
        else:
            b = fibonacci_rec(n - 2)
            fib_cache[n - 2] = b
        return a + b


def index(request):
    refrehs_authorization(request.session)
    return render(request, 'index.html')


def time_calculator(request):
    refrehs_authorization(request.session)
    response = {'result': False}
    if request.method == 'GET':
        response['form'] = TimeCalculatorForm
    elif request.method == 'POST':
        hours = int(request.POST.get('hours', 0))
        minutes = int(request.POST.get('minutes', 0))
        seconds = int(request.POST.get('seconds', 0))
        if hours != 0 or minutes != 0 or seconds != 0:
            params = {'hours': hours, 'minutes': minutes, 'seconds': seconds}
            time_result = requests.get('http://infralabs.cs.ui.ac.id:20062/api/time-calc', params)
            response.update(json.loads(time_result.text))
            response['result'] = True
            response['form'] = TimeCalculatorForm(initial={'hours': hours, 'minutes': minutes, 'seconds': seconds})
    else:
        return HttpResponseNotFound()
    return render(request, 'timeCalc.html', response)


def oauth(request):
    refrehs_authorization(request.session)
    if request.method == 'GET':
        context = {
            'form': OAuthForm
        }
        return render(request, 'oauth.html', context)
    elif request.method == 'POST':
        form = OAuthForm(request.POST)
        if form.is_valid():
            cleaned_form = form.cleaned_data
            payload = {
                'username': cleaned_form.get('username'),
                'password': cleaned_form.get('password'),
                'grant_type': 'password',
                'client_id': settings.INFRALABS_CLIENT_ID,
                'client_secret': settings.INFRALABS_CLIENT_SECRET,
            }
            oauth_request = requests.post('http://oauth.infralabs.cs.ui.ac.id/oauth/token/', data=payload)
            try:
                oauth_response = oauth_request.json()
            except ValueError:
                return HttpResponseBadRequest()
            if 'error' not in oauth_response:
                request.session['infralabs_access_token'] = oauth_response['access_token']
                request.session['infralabs_username'] = cleaned_form.get('username')
                return redirect('oauth-resource')
            else:
                return redirect('oauth')
        else:
            return redirect('oauth')
    else:
        return HttpResponseNotFound()


def oauth_resource(request):
    refrehs_authorization(request.session)
    if request.method == 'GET':
        if is_authorized(request.session):
            headers = {
                'Authorization': f'Bearer {request.session["infralabs_access_token"]}'
            }
            resource_request = requests.get('http://oauth.infralabs.cs.ui.ac.id/oauth/resource', headers=headers)
            if resource_request.status_code == 200:
                try:
                    data = resource_request.json()
                except ValueError:
                    return HttpResponseServerError()
                if 'access_token' in data:
                    data['access_token'] = data['access_token'][0:5] + '*' * (len(data['access_token']) - 5)
                    data['expires'] = f'{data["expires"]}'[0:-3]
                    response = {
                        'formatted': json.dumps(data, indent=4, sort_keys=True)
                    }
                    return render(request, 'oauthResult.html', response)
                else:
                    clear_authorization(request.session)
                    return HttpResponseForbidden()
            else:
                return HttpResponseServerError()
        else:
            clear_authorization(request.session)
            return HttpResponseForbidden()
    else:
        return HttpResponseNotFound()


def file_upload(request):
    refrehs_authorization(request.session)
    if request.method == 'GET':
        if 'id' in request.GET:
            temp = request.GET['id']
            context = {
                'form': FileUploadForm(initial={'hidden': temp}),
                'id': temp
            }
        else:
            context = {
                'form': FileUploadForm
            }
        return render(request, 'fileHosting.html', context)
    elif request.method == 'POST':
        if is_authorized(request.session):
            form = FileUploadForm(request.POST, request.FILES)
            if form.is_valid():
                header = {
                    'Authorization': f'Bearer {request.session["infralabs_access_token"]}'
                }
                metadata = {
                    'author': request.session['infralabs_username'],
                    'type': 'original'
                }
                if form.data['hidden']:
                    try:
                        query = FileStorage.objects.get(id=form.data['hidden'])
                    except FileStorage.DoesNotExist:
                        return HttpResponseNotFound()
                    query.original = request.FILES['file']
                    query.compressed = None
                    query.save()
                    metadata['fileId'] = query.id
                    metadata['action'] = 'u'
                else:
                    instance = FileStorage(
                        user_id=request.session['infralabs_username'],
                        original=request.FILES['file']
                    )
                    instance.save()
                    metadata['fileId'] = instance.id
                    metadata['action'] = 'c'
                requests.post('http://infralabs.cs.ui.ac.id:20063/api/metadata', headers=header, json=metadata)
                return redirect('file-list')
            else:
                return redirect('file-upload')
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseNotFound()


def file_list(request):
    refrehs_authorization(request.session)
    if request.method == 'GET':
        if is_authorized(request.session):
            context = {
                'files': FileStorage.objects.all()
            }
            return render(request, 'fileList.html', context)
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseNotFound()


def compress(request):
    refrehs_authorization(request.session)
    if request.method == 'POST':
        if is_authorized(request.session):
            try:
                query = FileStorage.objects.get(id=request.POST['pk'])
            except FileStorage.DoesNotExist:
                return HttpResponseNotFound()
            header = {
                'Authorization': f'Bearer {request.session["infralabs_access_token"]}'
            }
            file = {
                'file': query.original.file
            }
            compress_request = requests.post(
                'http://infralabs.cs.ui.ac.id:20062/api/compress',
                headers=header,
                files=file
            )
            if compress_request.status_code == 200:
                archive = SimpleUploadedFile(f'{get_file_name(query.original.name)}.zip', compress_request.content)
                query.compressed.save(f'{get_file_name(query.original.name)}.zip', archive)
                metadata = {
                    'fileId': query.id,
                    'action': 'c',
                    'author': request.session['infralabs_username'],
                    'type': 'compressed'
                }
                requests.post('http://infralabs.cs.ui.ac.id:20063/api/metadata', headers=header, json=metadata)
                return redirect('file-list')
            else:
                return redirect('file-list')
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseNotFound()


def download_file(request):
    refrehs_authorization(request.session)
    if request.method == 'GET':
        if is_authorized(request.session):
            if 'id' in request.GET and 'type' in request.GET:
                try:
                    query = FileStorage.objects.get(id=request.GET['id'])
                except FileStorage.DoesNotExist:
                    return HttpResponseNotFound()
                headers = {
                    'Authorization': f'Bearer {request.session["infralabs_access_token"]}'
                }
                metadata = {
                    'fileId': query.id,
                    'action': 'r',
                    'author': request.session['infralabs_username'],
                }
                if request.GET['type'] == 'original':
                    metadata['type'] = 'original'
                    requests.post('http://infralabs.cs.ui.ac.id:20063/api/metadata', headers=headers, json=metadata)
                    return FileResponse(query.original.file, as_attachment=True)
                else:
                    if query.compressed:
                        metadata['type'] = 'compressed'
                        requests.post('http://infralabs.cs.ui.ac.id:20063/api/metadata', headers=headers, json=metadata)
                        return FileResponse(query.compressed.file, as_attachment=True)
                    else:
                        return HttpResponseNotFound()
            else:
                return HttpResponseBadRequest()
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseNotFound()


def delete_file(request):
    refrehs_authorization(request.session)
    if request.method == 'POST':
        if is_authorized(request.session):
            if 'id' in request.POST and 'type' in request.POST:
                try:
                    query = FileStorage.objects.get(id=request.POST['id'])
                except FileStorage.DoesNotExist:
                    return HttpResponseNotFound()
                headers = {
                    'Authorization': f'Bearer {request.session["infralabs_access_token"]}'
                }
                metadata = {
                    'fileId': query.id,
                    'action': 'd',
                    'author': request.session['infralabs_username'],
                }
                if request.POST['type'] == 'original':
                    try:
                        query.original.delete(save=False)
                        query.compressed.delete(save=False)
                    except PermissionError:
                        return HttpResponse(status=503)
                    query.delete()
                    metadata['type'] = 'original'
                elif request.POST['type'] == 'compressed':
                    try:
                        query.compressed.delete(save=False)
                    except PermissionError:
                        return HttpResponse(status=503)
                    query.save()
                    metadata['type'] = 'compressed'
                else:
                    return HttpResponseBadRequest()
                requests.post('http://infralabs.cs.ui.ac.id:20063/api/metadata', headers=headers, json=metadata)
                return redirect('file-list')
            else:
                return HttpResponseBadRequest()
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseNotFound()


def audit_log(request):
    refrehs_authorization(request.session)
    if request.method == 'GET':
        if is_authorized(request.session):
            headers = {
                'Authorization': f'Bearer {request.session["infralabs_access_token"]}'
            }
            metadata_request = requests.get('http://infralabs.cs.ui.ac.id:20063/api/metadata', headers=headers)
            try:
                metadatas = metadata_request.json()['metadatas']
            except ValueError:
                return HttpResponseServerError()
            for metadata in metadatas:
                action = metadata['action']
                if action == 'c':
                    metadata['action'] = 'Create'
                elif action == 'r':
                    metadata['action'] = 'Read'
                elif action == 'u':
                    metadata['action'] = 'Update'
                elif action == 'd':
                    metadata['action'] = 'Delete'
                metadata['timestamp'] = datetime.strptime(metadata['timestamp'], '%Y-%m-%dT%H:%M:%S.%fZ') \
                    .strftime('%c')
                metadata['type'] = metadata['type'].capitalize()
            context = {
                'metadatas': metadatas
            }
            return render(request, 'auditLog.html', context)
        else:
            return HttpResponseForbidden()
    else:
        return HttpResponseNotFound()


def clear_token(request):
    if request.method == 'GET':
        clear_authorization(request.session)
        return redirect('oauth')
    else:
        return HttpResponseNotFound()


def multifile_compression(request):
    if request.method == 'GET':
        return render(request, 'multifileCompress.html')
    elif request.method == 'POST':
        routing_key = str(uuid.uuid4())
        headers = {
            'X-ROUTING-KEY': routing_key
        }
        files = {}
        for i, fileName in enumerate(request.FILES):
            file = request.FILES[fileName]
            files[file.name] = file.file
        compress_request = requests.post(
            'http://infralabs.cs.ui.ac.id:20063/api/multifile-compress',
            headers=headers,
            files=files
        )
        if compress_request.status_code == 200:
            return redirect(f'/multifile-compress-result?key={routing_key}')
        else:
            return redirect('multifile-compress')
    else:
        return HttpResponseNotFound()


def multifile_compress_result(request):
    if request.method == 'GET':
        return render(request, 'multifileCompressProgress.html')
    else:
        return HttpResponseNotFound()


def get_file_name(filename):
    temp = filename.split('/')
    return temp[-1]


def is_authorized(session):
    if 'infralabs_access_token' in session and 'infralabs_username' in session:
        return True
    else:
        return False


def token_is_valid(token):
    headers = {
        'Authorization': f'Bearer {token}'
    }
    resource_request = requests.get('http://oauth.infralabs.cs.ui.ac.id/oauth/resource', headers=headers)
    response = resource_request.json()
    if 'error' not in response:
        return True
    else:
        return False


def clear_authorization(session):
    try:
        del session['infralabs_access_token']
    except KeyError:
        pass
    try:
        del session['infralabs_username']
    except KeyError:
        pass


def refrehs_authorization(session):
    if is_authorized(session):
        if token_is_valid(session['infralabs_access_token']):
            return
        else:
            clear_authorization(session)
    else:
        clear_authorization(session)
