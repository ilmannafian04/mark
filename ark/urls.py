from django.urls import path

from ark import views

urlpatterns = [
    path('', views.index, name='index'),
    path('time-calc', views.time_calculator, name='time-calc'),
    path('oauth', views.oauth, name='oauth'),
    path('oauth-resource', views.oauth_resource, name='oauth-resource'),
    path('clear-oauth', views.clear_token, name='clear-oauth'),
    path('file-upload', views.file_upload, name='file-upload'),
    path('file-list', views.file_list, name='file-list'),
    path('file-list/compress', views.compress, name='compress-file'),
    path('file-list/download', views.download_file, name='download-file'),
    path('file-list/delete', views.delete_file, name='delete-file'),
    path('audit', views.audit_log, name='audit'),
    path('multifile-compress', views.multifile_compression, name='multifile-compress'),
    path('multifile-compress-result', views.multifile_compress_result, name='multifile-compress-result'),
    path('fibonacci', views.fibonacci, name='fibonacci'),
    path('downloader', views.downloader, name='downloader'),
    path('downloader/progress', views.downloader_progress, name='downloader-progress'),
    path('downloader/download', views.downloader_download),
]
