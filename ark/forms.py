from django import forms


class TimeCalculatorForm(forms.Form):
    hoursWidget = {
        'class': 'form-control',
        'id': 'inputHours'
    }
    minutesWidget = {
        'class': 'form-control',
        'id': 'inputMinutes'
    }
    secondsWidget = {
        'class': 'form-control',
        'id': 'inputSeconds'
    }
    hours = forms.IntegerField(initial=0, widget=forms.NumberInput(hoursWidget))
    minutes = forms.IntegerField(initial=0, widget=forms.NumberInput(minutesWidget))
    seconds = forms.IntegerField(initial=0, widget=forms.NumberInput(secondsWidget))


class OAuthForm(forms.Form):
    usernameWidget = {
        'class': 'form-control',
        'id': 'inputUsername'
    }
    passwordWidget = {
        'class': 'form-control',
        'id': 'inputPassword'
    }
    username = forms.CharField(widget=forms.TextInput(usernameWidget))
    password = forms.CharField(widget=forms.PasswordInput(passwordWidget))


class FileUploadForm(forms.Form):
    fileWidget = {
        'class': 'custom-file-input',
        'id': 'customFile',
        'onChange': 'changeName(this)',
        'name': 'file'
    }
    file = forms.FileField(widget=forms.FileInput(fileWidget))
    hidden = forms.CharField(required=False, widget=forms.HiddenInput())
