import os

from django.db import models


class FileStorage(models.Model):
    id = models.AutoField(primary_key=True)
    user_id = models.CharField(max_length=10)
    original = models.FileField(upload_to='upload/file_storage/original/')
    compressed = models.FileField(upload_to='upload/file_storage/compressed/', null=True)

    def original_name(self):
        return os.path.basename(self.original.name)


class DownloadStorage(models.Model):
    id = models.AutoField(primary_key=True)
    file = models.FileField(upload_to='upload/download_storage/')
